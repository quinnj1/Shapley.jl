# Shapley

[![docs](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/Shapley.jl/)
[![build status](https://img.shields.io/gitlab/pipeline/ExpandingMan/Shapley.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/Shapley.jl/-/pipelines)

A pure Julia package for computing [Shapley
values](https://en.wikipedia.org/wiki/Shapley_value) for machine learning features.
